<?php
if (isset($_GET['submit'])) {
   
    // Le formulaire a été soumis, vous pouvez appeler votre fonction ici
    //maFonction();
    var_dump($_GET['submit']);
    $poubelle = $_GET["poubelle"];
    $depart = $_GET["depart"];
    var_dump($poubelle);

    echo "Nom : " . $poubelle . "<br>";
    echo "Email : " . $depart . "<br>";

    function dijkstra($graph, $startNode, $endNode) {
        // Crée un tableau pour stocker les distances initiales et initialise-le à l'infini pour tous les nœuds sauf le nœud de départ.
        $distances = [];
        $visited = [];
        $previous = [];
        foreach ($graph as $node => $neighbors) {
            $distances[$node] = INF;
            $previous[$node] = null;
            $visited[$node] = false;
        }
        $distances[$startNode] = 0;
    
        // Tant que tous les nœuds n'ont pas été visités
        while (in_array(false, $visited, true)) {
            // Trouve le nœud non visité avec la distance minimale
            $closestNode = null;
            foreach ($graph[$startNode] as $neighbor => $distance) {
                if (!$visited[$neighbor]) {
                    if ($distances[$startNode] + $distance < $distances[$neighbor]) {
                        $distances[$neighbor] = $distances[$startNode] + $distance;
                        $previous[$neighbor] = $startNode;
                    }
                    if ($closestNode === null || $distances[$neighbor] < $distances[$closestNode]) {
                        $closestNode = $neighbor;
                    }
                }
            }
    
            // Marque le nœud comme visité
            $visited[$startNode] = true;
            $startNode = $closestNode;
        }
    
        // Construit le chemin le plus court
        $path = [];
        while ($endNode !== null) {
            array_unshift($path, $endNode);
            $endNode = $previous[$endNode];
        }
    
        return $path;
    }
    
    // Exemple d'utilisation
    $graph = [
        'A' => ['B' => 4, 'C' => 2],
        'B' => ['A' => 4, 'C' => 5, 'D' => 10],
        'C' => ['A' => 2, 'B' => 5, 'D' => 3],
        'D' => ['B' => 10, 'C' => 3],
    ];
    
    $startNode = 'A';
    $endNode = 'D';
    
    $shortestPath = dijkstra($graph, $startNode, $endNode);
    
    echo "Le chemin le plus court de $startNode à $endNode est : " . implode(' -> ', $shortestPath);
}