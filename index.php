<?php
// Initialisez les variables pour stocker les données du formulaire
$detail = "";
$poubelle = "";

// Vérifiez si le formulaire a été soumis
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Récupérez les données du formulaire
    $poubelle = $_POST["poubelle"];
    $depart = $_POST["depart"];

    // Traitez les données comme vous le souhaitez, par exemple, enregistrez-les dans une base de données ou effectuez d'autres opérations

    // Vous pouvez également rediriger l'utilisateur vers une autre page si nécessaire
    // header("Location: autre_page.php");
    // exit(); // Assurez-vous de terminer le script après la redirection
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MAP POUBELLE</title>
    <style>
        .section-map h1{
        color: white ;
        border: 1px solid #87eda3;
        border-radius: 15px;
        background-color: #43a35d;
        padding: 20px;
        box-shadow:  5px 5px #bec4c0;
        text-transform: uppercase;
        width: 100%;
        height: auto;
        margin-top: 25px;
    }
    </style>
</head>
<body>
    <section class="section-map container">
        <div class="row">
            <h1 class="text-center" style="text-align: center;">Bienvenue sur l'application de recherche de poubelle</h1>
            <h3 class="text-center" style="margin-top: 20px; margin-bottom:20px">Trouver le chemin le plus court pour accéder à une poubelle</h3>
        </div>

    </section>
    <section class="section-map container">
        <div style="width: 100%;">
            <iframe src="https://www.google.com/maps/d/embed?mid=1o5pItGtEJzfoYNWovkLe47u0PyVfK6Y&ehbc=2E312F&noprof=1" width="100%" height="480"></iframe>
        </div>
    </section>
    <section style="margin-top: 50px;" class="container">
    <div class="">
        
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST"  class="">
                <div class="mb-3 ">
                    <label for="exampleFormControlInput1" class="form-label">Point de départ*</label>
                    <input type="text" name="poubelle" class="form-control" id="exampleFormControlInput1" placeholder="Départ">
                </div >
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Cible*</label>
                    <input type="text" name="depart" class="form-control" id="exampleFormControlInput1" placeholder="Position de la poubelle">
                </div >

                <div class="mb-3 text-center">
                    <button type="submit" name="submit" class="btn btn-primary btn-lg">Search</button>
                </div>
            </form>
        </div>
    </section>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        echo '<h2 style="text-align:center">Résultat du traitement :</h2>';
       

        function dijkstra($graph, $startNode, $endNode) {
            // Crée un tableau pour stocker les distances initiales et initialise-le à l'infini pour tous les nœuds sauf le nœud de départ.
            $distances = [];
            $visited = [];
            $previous = [];
            foreach ($graph as $node => $neighbors) {
                $distances[$node] = INF;
                $previous[$node] = null;
                $visited[$node] = false;
            }
            $distances[$startNode] = 0;
        
            // Tant que tous les nœuds n'ont pas été visités
            while (in_array(false, $visited, true)) {
                // Trouve le nœud non visité avec la distance minimale
                $closestNode = null;
                foreach ($graph[$startNode] as $neighbor => $distance) {
                    if (!$visited[$neighbor]) {
                        if ($distances[$startNode] + $distance < $distances[$neighbor]) {
                            $distances[$neighbor] = $distances[$startNode] + $distance;
                            $previous[$neighbor] = $startNode;
                        }
                        if ($closestNode === null || $distances[$neighbor] < $distances[$closestNode]) {
                            $closestNode = $neighbor;
                        }
                    }
                }
        
                // Marque le nœud comme visité
                $visited[$startNode] = true;
                $startNode = $closestNode;
            }
        
            // Construit le chemin le plus court
            $path = [];
            while ($endNode !== null) {
                array_unshift($path, $endNode);
                $endNode = $previous[$endNode];
            }
        
            return $path;
        }
        
        // Exemple d'utilisation
        $graph = [
            'I' => ['G' => 1.45, 'C' => 4.70],
            'A' => ['C' => 1.04, 'G' => 2.80],
            'B' => ['E' => 2.76, 'G' => 4.5],
            'D' => ['B' => 10, 'C' => 3],
        ];
        // $graph = [
        //     'A' => ['B' => 4, 'C' => 2],
        //     'B' => ['A' => 4, 'C' => 5, 'D' => 10],
        //     'C' => ['A' => 2, 'B' => 5, 'D' => 3],
        //     'D' => ['B' => 10, 'C' => 3],
        // ];
        
        $startNode = $depart;
        $endNode = $poubelle;
        
        $shortestPath = dijkstra($graph, $startNode, $endNode);
        
        echo "Le chemin le plus court de $startNode à $endNode est : " . implode(' -> ', $shortestPath);
    }
    ?>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>
</html>